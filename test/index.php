<?php

   define((MOBILE), 8796507505);
   $name ="Ajay";
   $age =22;
   $company_name="wgbl ind pvt ltd";

   $datas=[
['name1'=>'ajay','sub'=>'math','marks'=>'80'],
['name1'=>'vijay','sub'=>'hindi','marks'=>'90'],
['name1'=>'sanjay','sub'=>'eng','marks'=>'70'],
['name1'=>'raju','sub'=>'sci','marks'=>'60'],
];

/*
   $string1="hiii";
   $string2="byyy";

   echo $string1.$string2;                  //conacanating two strings
*************************************************************************************
   $string="ajay";
   echo "my name is $string";              //output =>my name is ajay
   echo 'my name is $string';             //output =>my name is $string

   tip=> If we have use "" then the value of variable will be access easily,
   but in the '' it cant be happen
*************************************************************************************

   echo "heyy my name is \"whaaa\"";        //output=>heyy my name is "whaaa"
   echo 'heyy my name is "whaaa"';          // output=>heyy my name is "whaaa"
   tip=> here \  \ is work for escape the things
*************************************************************************************
String Function example:-
    $string="ajay";
    echo $string[1];
    echo strlen($string);
    echo strtoupper($string);
    echo str_word_count($string);
    echo str_replace('j', 'bh', $string);
*************************************************************************************
Numbers:-

$number=30.1;

echo floor($number);
echo ceil($number);
*************************************************************************************
Arrays:

//indexed Array
$array_name=['ajay','rushi','kajal'];

echo $array_name[2];

$array2=array('ajay','vijay','ram');
echo $array2[2];

$ages =[20,30,40];
print_r($ages);
**************************************************************************************
//multidiamensional Array

//$mul=['name'=>'ajay','name2'=>'vijay'];
$mul=array('name'=>'ajay','name2'=>'vijay');
print_r($mul);

**************************************************************************************
//Loops:

$ages=['12','13','15'];

for ($i=0;$i<count($ages);$i++)
{
  print_r($ages[$i]);
  echo "<br />";
}

foreach ($ages as $age) {
   print_r($age);
}

$data=[
['name1'=>'ajay','sub'=>'math','marks'=>'80'],
['name1'=>'vijay','sub'=>'hindi','marks'=>'90'],
['name1'=>'sanjay','sub'=>'eng','marks'=>'70'],
['name1'=>'raju','sub'=>'sci','marks'=>'60'],
];
echo "<br />";
for($i=0;$i<count($data);$i++)
{

  print_r($data[$i]['name1'].' '.$data[$i]['sub']);
  echo "<br />";
}

//while loop
$ages=['12','13','15'];
$i=0;
while($i<count($ages))
{
 echo $ages[$i];
  $i++;
}
************************************************************************************
//conditional Statements


$datas=[
['name1'=>'ajay','sub'=>'math','marks'=>'80'],
['name1'=>'vijay','sub'=>'hindi','marks'=>'90'],
['name1'=>'sanjay','sub'=>'eng','marks'=>'70'],
['name1'=>'raju','sub'=>'sci','marks'=>'60'],
];

foreach ($datas as $data) {
   if($data['marks']<90){
     echo $data['name1'].' '.$data['sub'].'<br />';
   }
}
**************************************************************************************


function formatProduct($subject){
  print_r("Subject name is {$subject['name1']} and marks is {$subject['marks']} <br />" );
}

formatProduct(['name1'=>'hindi','marks'=>20]);

// the use of retrun statement here is that ,

function formatProduct($subject){
  return("Subject name is {$subject['name1']} and marks is {$subject['marks']} <br />" );
}

$product_data=formatProduct(['name1'=>'hindi','marks'=>20]);
echo $product_data;
*****************************************************************************************
//Variable scope
1]local scope:-inside the function
2]Global scope:-outside the function

$age=20;
function sayHello(){
  global $age;          //global scope
  $name1='ajay';        //local scope
  echo "Hello $name1";
  echo "age is $age";
}
sayHello();

//tip :=> Here we updated/override global value as in local scope,
         if you passed the reffernce of global variable in function then the global valu bacome a change.

$age=20;
function sayHello($age){
  $age=21;

  echo "age is $age";                    //output => age is 21
}
sayHello($age);
echo $age;                              //output => age is 20

$age=20;
function sayHello(&$age){
  $age=21;

  echo "age is $age";                    //output => age is 21
}
sayHello($age);
echo $age;                              //output => age is 21

***************************************************************************************
*/
//include & require

//tip=> here when we use include and require then we can easily access another php file in prresent file,this is use powerfully in php.
// when we use include(filename.php) then file will be included,same as require(filename.php),the littlebit diff. is that if file does not exist then include have run rest of the code but require can get the error for that line and does not execute rest of code.

?>

<!DOCTYPE>
<html>
    <body>
      <h2>This is my first php example</h2>
       <div><?php  print "name: $name "; ?></div>
       <div> <?php echo "Age is:$age"; ?></div>
       <div> <?php echo "company name is:$company_name"; ?></div>
       <div> <?php echo "Mobile Number is:".MOBILE; ?></div>
        <div>
          <ul><?php foreach ($datas as $data) {  ?>
                <?php if ($data['marks']<90) {  ?>
                  <li><?php echo $data['name1'].' '.$data['sub'].'<br />'; ?></li>
                <?php } ?>
              <?php } ?>
          </ul>
        </div>
    </body>
</html>